# Visual Finance in Worldviz #

### Version ###
* 1.0.0

### Bitbucket ###
https://bitbucket.org/wyatt-hui-team/worldviz-finance

### Installation
Paste data folder to the root

### Team contacts
* Wyatt Hui (14093161d@connect.polyu.hk/wyatt.hui.1104@google.com)

### License
14093161D Hui Ka Wai
The Hong Kong Polytechnic University
