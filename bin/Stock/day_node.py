﻿from datetime import date

class DayNode:
  def __init__(self, params):
    dateDescription = map(int, params[0].split('-'))
    self.date = date(
      dateDescription[0],
      dateDescription[1],
      dateDescription[2]
    )
    self.open = float(params[1])
    self.high = float(params[2])
    self.low = float(params[3])
    self.close = float(params[4])
    self.adjClose = float(params[5])
    self.volume = float(params[6])

  def __str__(self):
    return ', '.join([
      self.date.__str__(),
      str(self.open),
      str(self.high),
      str(self.low),
      str(self.close),
      str(self.adjClose),
      str(self.volume)
    ])
