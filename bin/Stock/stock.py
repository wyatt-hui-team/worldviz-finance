﻿import math
import csv

import viz
import vizconnect
import vizshape

from utils import utils, constants
from day_node import DayNode
from helpers.text_description_helper import TextDescriptionHelper
from helpers.grid_line_drawing_helper import GridLineDrawingHelper
from helpers.node_drawing_helper import NodeDrawingHelper
from helpers.day_node_info_display_helper import DayNodeInfoDisplayHelper

STOCK_CONTAINER_SIZE = constants.STOCK_CONTAINER_SIZE
STOCK_FONT_SIZE = constants.STOCK_FONT_SIZE

class Stock:
  def __init__(self, name):
    path = constants.DATA_PATH + '/' + name + '.' + constants.FILE_EXTENSION
    dayNodeList = createDayNodeList(path)
    container = createContainer(STOCK_CONTAINER_SIZE, self)

    self._stockDict = {
      'name': name,
      'dayNodeList': dayNodeList,
      'containerSize': STOCK_CONTAINER_SIZE,
      'outlines': drawCubeByLine(STOCK_CONTAINER_SIZE, parent=container),
      'days': constants.STOCK_MINIMUM_DAYS,
      'toDate': dayNodeList[-1].date,
      'updatedParamDict': {},
      'textDescriptionHelper': TextDescriptionHelper(name, STOCK_CONTAINER_SIZE, container),
      'gridLineDrawingHelper': GridLineDrawingHelper(),
      'nodeDrawingHelper': NodeDrawingHelper(),
      'dayNodeInfoDisplayHelper': DayNodeInfoDisplayHelper()
    }

    self.container = container

    if len(dayNodeList) > 0:
      self._saveUpdatedParams(**updateParams(container, **self._stockDict))

  def highlight(self, point):
    stockDict = self._stockDict
    currentDayNodeList = stockDict['updatedParamDict']['currentDayNodeList']

    posDict = stockDict['gridLineDrawingHelper'].createPointingLines(
      point,
      **stockDict['updatedParamDict']
    )

    containerSizeX = stockDict['containerSize'][0]
    posXPercentage = (posDict['x'] + containerSizeX / 2) / containerSizeX
    index = len(currentDayNodeList) - 1
    if posXPercentage < 1:
      index = int(len(currentDayNodeList) * posXPercentage)
    dayNode = currentDayNodeList[index]

    stockDict['textDescriptionHelper'].createPointingTexts(
      posDict['x'],
      posDict['y'],
      dayNode,
      **stockDict['updatedParamDict']
    )
    stockDict['dayNodeInfoDisplayHelper'].display(dayNode)

  def resetPosition(self, position):
    setPositionForContainer(self.container, position)

  def shiftToDate(self, isMovingForward):
    dayNodeList = self._stockDict['dayNodeList']

    if len(dayNodeList) <= 0:
      return

    toDateIdx = getToDateIdx(dayNodeList, self._stockDict['toDate'])
    movingSteps = self._stockDict['days'] / constants.STOCK_MINIMUM_DAYS

    if isMovingForward:
      toDateIdx = min(toDateIdx + movingSteps, len(dayNodeList) - 1)
    else:
      toDateIdx = max(toDateIdx - movingSteps, 0)

    self._stockDict['toDate'] = dayNodeList[toDateIdx].date
    self._saveUpdatedParams(**updateParams(self.container, **self._stockDict))

  def zoom(self, isZoomOut):
    dayNodeList = self._stockDict['dayNodeList']

    if len(dayNodeList) <= 0:
      return

    days = self._stockDict['days']

    if isZoomOut:
      self._stockDict['days'] = days * 2 -1
      toDateIdx = getToDateIdx(dayNodeList, self._stockDict['toDate'])
      toDateIdx = min(toDateIdx + math.floor(days / 2), len(dayNodeList) - 1)
      self._stockDict['toDate'] = dayNodeList[int(toDateIdx)].date
    else:
      self._stockDict['days'] = (days + 1) / 2

    self._saveUpdatedParams(**updateParams(self.container, **self._stockDict))

  def _saveUpdatedParams(self, days, toDate, updatedParamDict, **kw):
    self._stockDict['days'] = days
    self._stockDict['toDate'] = toDate
    self._stockDict['updatedParamDict'] = updatedParamDict

  def remove(self):
    self._stockDict['textDescriptionHelper'].remove()
    self.container.remove()

  def __str__(self):
    return self._name

def createDayNodeList(path):
  with open(path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    return map(lambda row: DayNode(row), reader)

def createContainer(containerSize, klass):
  container = vizshape.addBox(size=containerSize)
  container.alpha(0)

  setPositionForContainer(container, 'front')

  container.stock = klass
  return container

def setPositionForContainer(container, position):
  transportPos = vizconnect.getTransport().getNode3d().getPosition()

  if position == 'front':
    container.setEuler(0, 0, 0)
  elif position == 'right':
    container.setEuler(90, 0, 0)

  container.setPosition([
    transportPos[0],
    constants.STOCK_DEFAULT_POSITION_Y,
    transportPos[2]
  ])
  container.setPosition(
    [0, 0, constants.STOCK_DEFAULT_POSITION_Z],
    viz.REL_LOCAL
  )

def drawCubeByLine(size, **kw):
  GRAY_COLOR = [0.2, 0.2, 0.2]
  size = [x / 2 for x in size]

  x = size[0]
  y = size[1]
  z = size[2]

  viz.startLayer(viz.LINE_STRIP)
  viz.vertex([-x, -y, -z])
  viz.vertex([ x, -y, -z])
  viz.vertex([ x,  y, -z])
  viz.vertex([-x,  y, -z])
  viz.vertex([-x, -y, -z])
  viz.endLayer(**kw)

  viz.startLayer(viz.LINE_STRIP)
  viz.vertexColor(GRAY_COLOR)
  viz.vertex([-x, -y, z])
  viz.vertex([ x, -y, z])
  viz.vertex([ x,  y, z])
  viz.vertex([-x,  y, z])
  viz.vertex([-x, -y, z])
  viz.endLayer(**kw)

  viz.startLayer(viz.LINES)
  viz.vertexColor(GRAY_COLOR)
  viz.vertex([-x, -y,  z])
  viz.vertex([-x, -y, -z])
  viz.vertex([ x, -y,  z])
  viz.vertex([ x, -y, -z])
  viz.vertex([-x, y, z])
  viz.vertex([-x, y, -z])
  viz.vertex([x, y, z])
  viz.vertex([x, y, -z])
  viz.endLayer(**kw)

def updateParams(container, toDate, days, dayNodeList, containerSize, textDescriptionHelper, gridLineDrawingHelper, nodeDrawingHelper, **kw):
  dayNodeListLength = len(dayNodeList)
  if dayNodeListLength <= 0:
    return

  days = max(days, constants.STOCK_MINIMUM_DAYS)
  days = min(days, dayNodeListLength)

  _dict = {}
  _dict['nodeWidth'] = containerSize[0] / days
  _dict['startingPointInX'] = -containerSize[0] / 2
  _dict['currentDayNodeList'] = getDayNodeList(dayNodeList, toDate, days)
  boundaryHighDict = getBoundaryInfo(_dict['currentDayNodeList'], 'high')
  boundaryLowDict = getBoundaryInfo(_dict['currentDayNodeList'], 'low')
  _dict['boundaryYDict'] = {
    'lowest': boundaryLowDict['lowest'],
    'lowestPadding': boundaryLowDict['lowestPadding'],
    'highest': boundaryHighDict['highest'],
    'highestPadding': boundaryHighDict['highestPadding'],
    'differenceInPadding': boundaryHighDict['highestPadding'] - boundaryLowDict['lowestPadding']
  }
  _dict['boundaryVolumeDict'] = getBoundaryInfo(_dict['currentDayNodeList'], 'volume')
  _dict['startingPointInY'] = -containerSize[1] / 2
  _dict['startingPointInZ'] = -containerSize[2] / 2
  _dict['containerSize'] = containerSize
  calDayNodePos = defCalDayNodePos(**_dict)
  _dict['dayNodePosDictList'] = map(
    lambda (i, val): calDayNodePos(i ,val),
    enumerate(_dict['currentDayNodeList'])
  )
  _dict['parent'] = container

  textDescriptionDict = textDescriptionHelper.updateTexts(**_dict)
  gridLineDrawingHelper.updateLines(textDescriptionDict, **_dict)
  nodeDrawingHelper.updateNodes(**_dict)

  return {
    'days': days,
    'toDate': toDate,
    'updatedParamDict': _dict
  }

def getDayNodeList(dayNodeList, toDate, days):
  toDateIdx = getToDateIdx(dayNodeList, toDate)
  return dayNodeList[toDateIdx - days + 1:toDateIdx + 1]

def getToDateIdx(dayNodeList, toDate):
  for i, dayNode in enumerate(dayNodeList):
    if dayNode.date == toDate:
      return i

def getBoundaryInfo(dayNodeList, attribute):
  firstDayValue = dayNodeList[0].__dict__[attribute]
  lowest = firstDayValue
  highest = firstDayValue

  for dayNode in dayNodeList:
    val = dayNode.__dict__[attribute]
    if lowest > val:
      lowest = val
    if highest < val:
      highest = val

  boundaryPaddingMultiplier = constants.STOCK_BOUNDARY_PADDING_MULTIPLIER
  lowestPadding = lowest / boundaryPaddingMultiplier
  highestPadding = highest * boundaryPaddingMultiplier

  return {
    'lowest': lowest,
    'lowestPadding': lowestPadding,
    'highest': highest,
    'highestPadding': highestPadding,
    'differenceInPadding': highestPadding - lowestPadding
  }

def defCalDayNodePos(nodeWidth, startingPointInX, boundaryYDict, boundaryVolumeDict, containerSize, startingPointInY, startingPointInZ, **kw):
  def calDayNodePos(i, val):
    return {
      'x': startingPointInX + nodeWidth / 2 + i * nodeWidth,
      'open': calDataPos(val.open, boundaryYDict, containerSize[1], startingPointInY),
      'high': calDataPos(val.high, boundaryYDict, containerSize[1], startingPointInY),
      'low': calDataPos(val.low, boundaryYDict, containerSize[1], startingPointInY),
      'close': calDataPos(val.close, boundaryYDict, containerSize[1], startingPointInY),
      'volume': calDataPos(val.volume, boundaryVolumeDict, -(containerSize[2] - nodeWidth), -(startingPointInZ + nodeWidth / 2))
    }
  return calDayNodePos

def calDataPos(value, boundaryDict, length, startingPoint):
  return startingPoint + length * (value - boundaryDict['lowestPadding']) / boundaryDict['differenceInPadding']
