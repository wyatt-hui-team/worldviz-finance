﻿import viz
import vizconnect
import vizshape

from utils import utils, observer, constants

class EnvironmentManager:
  def __init__(self, stockSelectDialog):
    self._stockSelectDialog = stockSelectDialog

    initViz()

    observer.subscribe(constants.EVENT_KEYDOWN, self._onKeyDown)
    observer.subscribe(constants.EVENT_STOCK_SELECT_DIALOG_SHOW, onStockSelectDialogShow)
    observer.subscribe(constants.EVENT_STOCK_SELECT_DIALOG_CLOSE, onStockSelectDialogClose)

  def _onKeyDown(self, key):
    toolManager = self._toolManager

    if self._stockSelectDialog.getVisible() is False and utils.isTriggering() is False:
      if key in constants.KEY_MOVE_LEFT:
        toolManager.changeTool(False)
      elif key in constants.KEY_MOVE_RIGHT:
        toolManager.changeTool(True)

  def setToolManager(self, toolManager):
    self._toolManager = toolManager

def initViz():
  viz.setMultiSample(4)
  vizconnect.go('base_vizconnect_config/' + constants.BASE_VIZCONNECT_CONFIG.lower() + '.py')
  viz.fov(60)
  viz.go()

  viz.mouse.setVisible(viz.OFF)

  viz.addChild(constants.SKYBOX)
  vizshape.addGrid(
    size=[750.0, 750.0],
    step=5.0,
    boldStep=10.0,
    color=[0.2, 0.2, 0.2]
  )

def onStockSelectDialogShow(data):
  viz.mouse.setVisible(viz.ON)
  vizconnect.getTransport().setEnabled(False)

def onStockSelectDialogClose(data):
  viz.mouse.setVisible(viz.OFF)
  vizconnect.getTransport().setEnabled(True)
