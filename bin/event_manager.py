﻿import viz
import vizconnect

from utils import utils, observer, constants

class EventManager:
  def __init__(self):
    self.joystickPosition = [0.0, 0.0]

    viz.callback(viz.UPDATE_EVENT, self._onUpdate)
    viz.callback(viz.KEYDOWN_EVENT, self._onKeyDown)
    viz.callback(viz.SENSOR_DOWN_EVENT, self._onSensorDownEvent)

  def _onUpdate(self, e):
    observer.publish(constants.EVENT_UPDATE, e)
    if constants.BASE_VIZCONNECT_CONFIG == 'CORNER_CAVE':
      self._onJoystickPositionUpdate();

  def _onJoystickPositionUpdate(self):
    rawInput = vizconnect.getConfiguration().getRawDict('input')
    currentJoystickPosition = utils.getRHandInput().getJoystickPosition()

    eventX = None
    if currentJoystickPosition[0] > 0 and self.joystickPosition[0] <= 0:
      eventX = constants.SENSOR_JOYSTICK_RIGHT
    elif currentJoystickPosition[0] < 0 and self.joystickPosition[0] >= 0:
      eventX = constants.SENSOR_JOYSTICK_LEFT
    if eventX != None:
      observer.publish(constants.EVENT_KEYDOWN, eventX)

    eventY = None
    if currentJoystickPosition[1] > 0 and self.joystickPosition[1] <= 0:
      eventY = constants.SENSOR_JOYSTICK_DOWN
    elif currentJoystickPosition[1] < 0 and self.joystickPosition[1] >= 0:
      eventY = constants.SENSOR_JOYSTICK_UP
    if eventY != None:
      observer.publish(constants.EVENT_KEYDOWN, eventY)

    self.joystickPosition = currentJoystickPosition

  def _onKeyDown(self, key):
    observer.publish(constants.EVENT_KEYDOWN, key)

  def _onSensorDownEvent(self, e):
    observer.publish(constants.EVENT_KEYDOWN, e.button)
