﻿import viz
import vizdlg
import vizinfo

from utils import observer, constants
from tool_manager import ToolManager

class KeyHintsDisplayHelper:
  def __init__(self):
    self._infoPanel = vizinfo.InfoPanel(
      text='',
      align=viz.ALIGN_LEFT_BOTTOM,
      icon=False,
      title='',
      skin=vizdlg.ModernSkin
    )
    self._infoPanel._titleBar.setTheme(viz.getTheme())

    observer.subscribe(constants.EVENT_TOOL_MANAGER_INIT_TOOL, self._onToolManagerInitTool)

  def _onToolManagerInitTool(self, type):
    if type == ToolManager.TYPE_GRABBER:
      title = 'Grabber'
      text = 'Trigger: Grab stock'
      text += '\n\nOn grabbing:'
      text += '\nUp: Move to front'
      text += '\nRight: Move to right'
    elif type == ToolManager.TYPE_HIGHLIGHTER:
      title = 'Highlighter'
      text = 'Trigger: Highlight stock'
      text += '\n\nOn highlighting:'
      text += '\nLeft/Right: Shift date'
      text += '\nUp/Down: Zoom in/out'

    text += '\nCenter: Remove stock'

    text += '\n\nCenter: Open stock selector'
    text += '\nLeft/Right: Change tool'

    self._infoPanel.setTitle(title)
    self._infoPanel.setText(text)
