﻿from stock.stock_select_dialog import StockSelectDialog
from environment_manager import EnvironmentManager
from stock.stock_manager import StockManager
from tool_manager import ToolManager
from key_hints_display_helper import KeyHintsDisplayHelper
from event_manager import EventManager

class Main:
  def __init__(self):
    # Nice to have
    # TODO: Key hints key/value seperation
    # TODO: hide key hints panel when stock selecting
    # TODO: [Pencil] Add pencil
    # TODO: Add average line
    # TODO: Add animation for create/delete stock

    stockSelectDialog = StockSelectDialog()
    environmentManager = EnvironmentManager(stockSelectDialog)
    stockManager = StockManager(stockSelectDialog)
    KeyHintsDisplayHelper()
    toolManager = ToolManager(0, stockManager)
    EventManager()

    environmentManager.setToolManager(toolManager);
    stockManager.setToolManager(toolManager);

Main()
