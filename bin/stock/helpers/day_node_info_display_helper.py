﻿import viz
import vizdlg
import vizinfo
import viztask

class DayNodeInfoDisplayHelper:
  DAY_NODE_INFO_DICTS = [{
    'ALIGN': viz.ALIGN_LEFT_TOP,
    'WINDOW_IDX': 0
  }, {
    'ALIGN': viz.ALIGN_RIGHT_TOP,
    'WINDOW_IDX': 1
  }]

  def __init__(self):
    self._infoPanels = []
    for dict in self.DAY_NODE_INFO_DICTS:
      infoPanel = vizinfo.InfoPanel(
        text='',
        align=dict['ALIGN'],
        icon=False,
        window=viz.VizWindow(dict['WINDOW_IDX']),
        title='Day info.',
        skin=vizdlg.ModernSkin
      )
      infoPanel.setPanelVisible(viz.OFF, animate=False)
      infoPanel._titleBar.setTheme(viz.getTheme())
      self._infoPanels.append(infoPanel)

  def display(self, dayNode):
    infoPanels = self._infoPanels
    for infoPanel in infoPanels:
      infoPanel.setText(createText(dayNode))
      infoPanel.setPanelVisible(viz.ON, animate=False)

      def _hiddenInfoPanel(infoPanel):
        yield
        infoPanel.setPanelVisible(viz.OFF, animate=False)
      viztask.schedule(_hiddenInfoPanel(infoPanel))

def createText(dayNode):
  text = 'Date: '
  text += dayNode.date.__str__()
  text += '\nOpen: '
  text += str(dayNode.open)
  text += '\nHigh: '
  text += str(dayNode.high)
  text += '\nLow: '
  text += str(dayNode.low)
  text += '\nClose: '
  text += str(dayNode.close)
  text += '\nVolume: '
  text += str(dayNode.volume)
  return text
