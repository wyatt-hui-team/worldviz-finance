﻿import viz
import viztask

from utils import constants

class GridLineDrawingHelper:
  def __init__(self):
    self._gridLineDict = {
      'priceLines': []
    }

  def updateLines(self, textDescriptionDict, **kw):
    self._removeLines()
    self._createPriceLines(textDescriptionDict, **kw)

  def _removeLines(self):
    for lines in self._gridLineDict.itervalues():
      for line in lines:
        line.remove()

  def _createPriceLines(self, textDescriptionDict, startingPointInX, startingPointInZ, parent, **kw):
    STOCK_GRID_LINE_COLOR = constants.STOCK_GRID_LINE_COLOR
    for priceDict in textDescriptionDict['priceDicts']:
      posY = priceDict['posY']

      viz.startLayer(viz.LINES)
      viz.vertexColor(STOCK_GRID_LINE_COLOR)
      viz.vertex([-startingPointInX, posY, startingPointInZ])
      viz.vertex([startingPointInX, posY, startingPointInZ])
      self._gridLineDict['priceLines'].append(viz.endLayer(parent=parent))

  def createPointingLines(self, point, containerSize, parent, **kw):
    viz.startLayer(viz.POINTS)
    viz.vertex([0, 0, 0])
    center = viz.endLayer(parent=parent)
    center.setPosition(point, viz.ABS_GLOBAL)

    centerPosition = center.getPosition()

    viz.startLayer(viz.LINES)
    containerRadiusY = containerSize[1] / 2
    viz.vertex([0, -containerRadiusY, 0])
    viz.vertex([0, containerRadiusY, 0])
    posY = centerPosition[1]
    horizontalLine = viz.endLayer(parent=center)
    horizontalLine.setPosition(
      [0, -posY, 0],
      viz.REL_LOCAL
    )

    viz.startLayer(viz.LINES)
    containerRadiusX = containerSize[0] / 2
    viz.vertex([-containerRadiusX, 0, 0])
    viz.vertex([containerRadiusX, 0, 0])
    posX = centerPosition[0]
    verticalLine = viz.endLayer(parent=center)
    verticalLine.setPosition(
      [-posX, 0, 0],
      viz.REL_LOCAL
    )

    def _removeCenter(center):
      yield
      if center != None:
        center.remove()

    viztask.schedule(_removeCenter(center))

    return {
      'x': posX,
      'y': posY
    }
