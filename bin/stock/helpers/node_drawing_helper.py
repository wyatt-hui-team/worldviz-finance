﻿import viz
import vizshape

from utils import utils, constants

class NodeDrawingHelper:
  def __init__(self):
    self._candles = []
    self._line = None

  def updateNodes(self, currentDayNodeList, **kw):
    removeLine(self._line)
    removeCandles(self._candles)

    if len(currentDayNodeList) <= constants.STOCK_LINE_CANDLES_DAYS_THRESHOLD:
      self._candles = createCandles(**kw)
    else:
      self._line = createLine(**kw)

def removeLine(line):
  if line is not None:
    line.remove()

def removeCandles(candles):
  for candle in candles:
    candle.remove()

def createCandles(nodeWidth, parent, dayNodePosDictList=[], **kw):
  candles = []

  if len(dayNodePosDictList) <= 0:
    return candles

  stockRealBodySize = nodeWidth * constants.STOCK_REAL_BODY_SIZE_MULTIPLIER
  stockShoadowSize = nodeWidth * constants.STOCK_SHADOW_SIZE_MULTIPLIER

  for dayNodePosDict in dayNodePosDictList:
    isIncreasing = dayNodePosDict['close'] >= dayNodePosDict['open']
    if isIncreasing:
      upperBase = dayNodePosDict['close']
      lowerBase = dayNodePosDict['open']
      color = constants.STOCK_COLOR_INCREASING
    else:
      upperBase = dayNodePosDict['open']
      lowerBase = dayNodePosDict['close']
      color = constants.STOCK_COLOR_DECREASING

    posY = (upperBase + lowerBase) / 2
    posZ = dayNodePosDict['volume']

    viz.startLayer(viz.POINTS)
    viz.vertex(dayNodePosDict['x'], posY, posZ)
    center = viz.endLayer(parent=parent)

    addBox = defAddBox(
      dayNodePosDict['x'],
      posZ,
      color,
      center
    )

    realBody = addBox(
      stockRealBodySize,
      upperBase - lowerBase,
      vizshape.ALIGN_CENTER,
      posY
    )
    realBody.disable(viz.LIGHTING)

    upperShadow = addBox(
      stockShoadowSize,
      dayNodePosDict['high'] - upperBase,
      vizshape.ALIGN_MIN,
      upperBase
    )
    upperShadow.disable(viz.LIGHTING)

    lowerShadow = addBox(
      stockShoadowSize,
      lowerBase - dayNodePosDict['low'],
      vizshape.ALIGN_MAX,
      lowerBase
    )
    lowerShadow.disable(viz.LIGHTING)

    candles.append(center)

  return candles

def defAddBox(posX, posZ, color, parent):
  def addBox(sizeXZ, sizeY, yAlign, posY):
    box = vizshape.addBox(
      size=[sizeXZ, sizeY, sizeXZ],
      yAlign=yAlign,
      pos=[posX, posY, posZ],
      color=color,
      parent=parent
    )
    return box
  return addBox

def createLine(containerSize, parent, dayNodePosDictList=[], **kw):
  if len(dayNodePosDictList) <= 0:
    return

  viz.startLayer(viz.LINE_STRIP)
  viz.lineWidth(containerSize[1])

  for dayNodePosDict in dayNodePosDictList:
    viz.vertex(dayNodePosDict['x'], dayNodePosDict['close'], dayNodePosDict['volume'])

  return viz.endLayer(parent=parent)
