﻿import math

import viz
import viztask

from utils import utils, constants

STOCK_FONT_SIZE = constants.STOCK_FONT_SIZE
SEPARATOR = '|'

class TextDescriptionHelper:
  def __init__(self, name, containerSize, parent):
    self._nameTextDict = createNameText(name, containerSize, parent)
    self._updatableDict = {
      'dayMonthDicts': [],
      'yearDicts': [],
      'yearSeparatorDicts': [],
      'priceDicts': []
    }

  def updateTexts(self, **kw):
    removeDescriptionDicts(self._updatableDict.itervalues())

    self._updatableDict['dayMonthDicts'] = createDayMonthDicts(**kw)
    yearAttrDict = createYearAttrDict(**kw)
    self._updatableDict['yearDicts'] = yearAttrDict['yearDicts']
    self._updatableDict['yearSeparatorDicts'] = yearAttrDict['yearSeparatorDicts']
    self._updatableDict['priceDicts'] = createPriceDicts(**kw)
    return self._updatableDict

  def createPointingTexts(self, posX, posY, dayNode, **kw):
    priceDict = createPricePointingText(posY, **kw)
    dayMonthDict = createDayMonthPointingText(posX, dayNode, **kw)

    def _removePointingTexts():
      yield
      removeDescriptionDicts([[priceDict, dayMonthDict]])

    viztask.schedule(_removePointingTexts())

  def remove(self):
    removeDescriptionDicts([[self._nameTextDict]])
    removeDescriptionDicts(self._updatableDict.itervalues())

def createNameText(text, containerSize, parent):
  return createDescriptionDict(
    text,
    viz.ALIGN_LEFT_BOTTOM,
    [
      -containerSize[0] / 2,
      containerSize[1] / 2 + STOCK_FONT_SIZE[1],
      -containerSize[2] / 2
    ],
    parent
  )

def createDayMonthDicts(currentDayNodeList, dayNodePosDictList, **kw):
  dicts = []
  spacing = len(currentDayNodeList) / constants.STOCK_MINIMUM_DAYS
  createDayMonthDescription = defCreateDayMonthDescription(**kw)

  for idx, dayNode in enumerate(currentDayNodeList):
    if idx % spacing == 0:
      date = dayNode.date
      dicts.append(createDayMonthDescription(
        createDayMonthString(date),
        dayNodePosDictList[idx]['x']
      ))
  return dicts

def createDayMonthPointingText(posX, dayNode, **kw):
  createDayMonthDescription = defCreateDayMonthDescription(**kw)
  return createDayMonthDescription(
    createDayMonthString(dayNode.date),
    posX
  )

def defCreateDayMonthDescription(containerSize, **kw):
  align = viz.ALIGN_CENTER_TOP
  posY = -(containerSize[1] / 2 + STOCK_FONT_SIZE[1])
  posZ = -containerSize[2] / 2

  def createDayMonthDescription(text, posX):
    return createDescriptionDict(text, align, [posX, posY, posZ], **kw)
  return createDayMonthDescription

def createDayMonthString(date):
  return str(date.month) + '/' + str(date.day)

def createYearAttrDict(nodeWidth, currentDayNodeList, containerSize, dayNodePosDictList, **kw):
  yearDicts = []
  yearSeparatorDicts = []
  yearStartingIdxDict = {}
  currentDayNodeListLength = len(currentDayNodeList)
  createYearAttrDescription = defCreateYearAttrDescription(
    -(containerSize[1] / 2 + constants.STOCK_FONT_SIZE[1] * 2),
    -containerSize[2] / 2,
    **kw
  )

  for idx, dayNode in enumerate(currentDayNodeList):
    year = dayNode.date.year
    if yearStartingIdxDict.get(year) is None:
      yearStartingIdxDict[year] = idx

  for year, idx in sorted(yearStartingIdxDict.iteritems(), reverse=True):
    nextYearIdx = yearStartingIdxDict.get(year + 1)
    if nextYearIdx is None:
      nextYearIdx = currentDayNodeListLength
    middleIdx = (nextYearIdx - idx - 1) / 2

    yearSeparatorDicts.append(createYearAttrDescription(
      SEPARATOR,
      dayNodePosDictList[idx]['x'] - nodeWidth / 2
    ))

    yearDicts.append(createYearAttrDescription(
      str(year),
      dayNodePosDictList[idx + middleIdx]['x']
    ))

  yearSeparatorDicts.append(createYearAttrDescription(
    SEPARATOR,
    dayNodePosDictList[currentDayNodeListLength - 1]['x'] + nodeWidth / 2
  ))

  return {
    'yearDicts': yearDicts,
    'yearSeparatorDicts': yearSeparatorDicts
  }

def defCreateYearAttrDescription(posY, posZ, **kw):
  align = viz.ALIGN_CENTER_TOP

  def createYearAttrDescription(text, posX):
    return createDescriptionDict(text, align, [posX, posY, posZ], **kw)
  return createYearAttrDescription

def createPriceDicts(boundaryYDict, containerSize, **kw):
  dicts = []
  digits = utils.countDigits(boundaryYDict['highestPadding'])
  stop = math.pow(10, digits)
  steps = stop * 0.05
  calPosY = defCalPosY(boundaryYDict, containerSize, **kw)
  createPriceDescription = defCreatePriceDescription(containerSize, **kw)

  for price in utils.frange(0, int(stop), steps):
    posY = calPosY(price)
    if abs(posY) <= containerSize[1] / 2:
      dict = createPriceDescription(str(price), posY)
      dict['posY'] = posY
      dicts.append(dict)

  return dicts

def defCalPosY(boundaryYDict, containerSize, startingPointInY, **kw):
  def calPosY(value):
    return startingPointInY + containerSize[1] * (value - boundaryYDict['lowestPadding']) / boundaryYDict['differenceInPadding']
  return calPosY

def createPricePointingText(posY, boundaryYDict, containerSize, **kw):
  containerSizeY = containerSize[1]
  posYPercentage = (posY + containerSizeY / 2) / containerSizeY
  price = posYPercentage * boundaryYDict['differenceInPadding'] + boundaryYDict['lowestPadding']
  createPriceDescription = defCreatePriceDescription(containerSize, **kw)
  return createPriceDescription(str('{0:.4f}'.format(price)), posY)

def defCreatePriceDescription(containerSize, **kw):
  align = viz.ALIGN_LEFT_CENTER
  posX = containerSize[0] / 2 + STOCK_FONT_SIZE[1] * 2
  posZ = -containerSize[2] / 2

  def createPriceDescription(text, posY):
    return createDescriptionDict(text, align, [posX, posY, posZ], **kw)
  return createPriceDescription

def createDescriptionDict(text, align, preTrans, parent, **kw):
  vizText = viz.addText(text, scale=STOCK_FONT_SIZE, align=align)
  vizText.disable(viz.LIGHTING)
  link = viz.link(parent, vizText)
  link.preTrans(preTrans)

  return {
    'text': vizText,
    'link': link
  }

def removeDescriptionDicts(descriptionDictList):
  for descriptionDicts in descriptionDictList:
    for descriptionDict in descriptionDicts:
      descriptionDict['link'].remove()
      descriptionDict['text'].remove()
