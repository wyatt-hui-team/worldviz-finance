﻿import viztask
from tools import grabber, highlighter

from utils import utils, observer, constants
from stock import Stock

class StockManager:
  def __init__(self, stockSelectDialog):
    self.stockContainers = []
    self._stockSelectDialog = stockSelectDialog

    observer.subscribe(constants.EVENT_KEYDOWN, self._onKeyDown)
    observer.subscribe(constants.EVENT_TOOL_MANAGER_HIGHLIGHTER_INTERSECT, onToolManagerHighlighterIntersect)

  def _onKeyDown(self, key):
    toolManager = self._toolManager

    currentToolType = toolManager.getCurrentToolType()
    eventAttrDict = toolManager.toolDict['eventAttrDict']
    isGrabber = currentToolType == toolManager.TYPE_GRABBER
    isHightlighter = currentToolType == toolManager.TYPE_HIGHLIGHTER
    event = None
    container = None
    stock = None

    if isGrabber:
      event = eventAttrDict.get(grabber.UPDATE_INTERSECTION_EVENT)
    elif isHightlighter:
      event = eventAttrDict.get(highlighter.HIGHLIGHT_EVENT)

    if event != None:
      container = event.new
      if container != None:
        stock = container.stock

    if self._stockSelectDialog.getVisible() is False:
      if utils.isTriggering():
        if stock != None:
          if isGrabber:
            if key in constants.KEY_MOVE_UP:
              self._resetPositionForStock(stock, 'front')
            elif key in constants.KEY_MOVE_RIGHT:
              self._resetPositionForStock(stock, 'right')
          elif isHightlighter:
            if key in constants.KEY_MOVE_LEFT:
              stock.shiftToDate(False)
            elif key in constants.KEY_MOVE_RIGHT:
              stock.shiftToDate(True)
            elif key in constants.KEY_MOVE_UP:
              stock.zoom(False)
            elif key in constants.KEY_MOVE_DOWN:
              stock.zoom(True)
        if key in constants.KEY_CENTER and container != None:
          self._deleteStock(container)
      elif key in constants.KEY_CENTER:
        viztask.schedule(utils.showDialog(self._stockSelectDialog, self._createStock))

  def _resetPositionForStock(self, stock, position):
    toolManager = self._toolManager

    toolManager.toolDict['restFn']()
    stock.resetPosition(position)

  def _deleteStock(self, container):
    toolManager = self._toolManager

    stock = container.stock
    self.stockContainers.remove(container)
    toolManager.setItemsToTool(self.stockContainers)
    stock.remove()
    del stock

  def _createStock(self, dialog):
    name = dialog.getSelectedName()

    if name is None:
      return

    stock = Stock(name)
    self.stockContainers.append(stock.container)
    self._toolManager.setItemsToTool(self.stockContainers)

  def setToolManager(self, toolManager):
    self._toolManager = toolManager

def onToolManagerHighlighterIntersect(intersect):
  if intersect == None:
    return

  stock = intersect.object.stock
  if stock != None:
    if utils.isTriggering():
      stock.highlight(intersect.point)
