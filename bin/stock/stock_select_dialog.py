﻿import string
from os import path, listdir

import viz
import vizdlg

from utils import observer, constants

class StockSelectDialog(vizdlg.Dialog):
  COL_SIZE = 14

  def __init__(self, title='Select stock', screenAlign=viz.ALIGN_LEFT_TOP,**kw):
    vizdlg.Dialog.__init__(self, title=title, screenAlign=screenAlign, skin=vizdlg.ModernSkin,**kw)
    self._titleBar.setTheme(viz.getTheme())

    fileNames = getFileNames()
    fileNamesDict = createFileNamesDict(fileNames)
    stocksDict = self._createStocksDict(fileNamesDict)
    gridPanelDict = createGridPanelDict(stocksDict)

    self._stockSelectDialogDict = {
      'fileNames': fileNames,
      'stocksDict': stocksDict,
      'gridPanelDict': gridPanelDict,
      'tabPanel': createTabPanel(gridPanelDict),
      'currentStockDictDict': None
    }

    self.content.addItem(self._stockSelectDialogDict['tabPanel'])

    self.accept.message('Create(Trigger)')
    self.cancel.message('Cancel(Center)')

  def _createStocksDict(self, fileNamesDict):
    COL_SIZE = self.COL_SIZE

    stocksDict = {}
    for char in sorted(fileNamesDict.iterkeys()):
      rows = []
      for idx, fileName in enumerate(fileNamesDict[char]):
        if idx % COL_SIZE == 0:
          cols = []
        radioBtn = viz.addRadioButton(char)
        onRadioBtnClicked = self._defOnRadioBtnClicked(char, len(cols), len(rows))
        radioBtn.addEventCallback(viz.BUTTON_EVENT, onRadioBtnClicked)
        cols.append({
          'radioBtn': radioBtn,
          'fileName': fileName
        })
        if idx % COL_SIZE == COL_SIZE - 1 or idx == len(fileNamesDict[char]) - 1:
          rows.append(cols)
      stocksDict[char] = rows
    return stocksDict

  def _defOnRadioBtnClicked(self, char, x, y):
    def onRadioBtnClicked(btn, state):
      self._stockSelectDialogDict['currentStockDictDict'][char] = {
        'x': x,
        'y': y
      }
    return onRadioBtnClicked

  def onShow(self):
    super(StockSelectDialog, self).onShow()
    self._initDialog(**self._stockSelectDialogDict)

    observer.publish(constants.EVENT_STOCK_SELECT_DIALOG_SHOW)
    self._onKeyDownEvent = observer.subscribe(constants.EVENT_KEYDOWN, self._onKeyDown, usePrepend=True)

  def _initDialog(self, fileNames, stocksDict, gridPanelDict, tabPanel, **kw):
    if len(fileNames) <= 0:
      return

    gridPanelDictKeys = sorted(gridPanelDict.iterkeys())
    self._stockSelectDialogDict['currentStockDictDict'] = {}
    for key in gridPanelDictKeys:
      stocksDict[key][0][0]['radioBtn'].set(True)
      self._stockSelectDialogDict['currentStockDictDict'][key] = {
        'x': 0,
        'y': 0
      }

    tabPanel.selectPanel(gridPanelDict[gridPanelDictKeys[0]])

  def onClose(self):
    super(StockSelectDialog, self).onClose()

    observer.unsubscribe(self._onKeyDownEvent)
    observer.publish(constants.EVENT_STOCK_SELECT_DIALOG_CLOSE)

  def _onKeyDown(self, key):
    kw = self._stockSelectDialogDict

    if key in constants.KEY_MOVE_LEFT:
      selectGrid('left', **kw)
    elif key in constants.KEY_MOVE_RIGHT:
      selectGrid('right', **kw)
    elif key in constants.KEY_MOVE_UP:
      selectGrid('up', **kw)
    elif key in constants.KEY_MOVE_DOWN:
      selectGrid('down', **kw)
    elif key in constants.KEY_TURN_LEFT:
      selectTab(False, **kw)
    elif key in constants.KEY_TURN_RIGHT:
      selectTab(True, **kw)
    elif key in constants.KEY_TRIGGER:
      self.doAccept()
    elif key in constants.KEY_CENTER:
      self.doCancel()

  def getSelectedName(self):
    _dict = self._stockSelectDialogDict

    if len(_dict['fileNames']) <= 0:
      return

    currentChar = getSelectedPanelChar(**_dict)
    currentStockDict = _dict['currentStockDictDict'][currentChar]
    stocks = _dict['stocksDict'][currentChar]
    return stocks[currentStockDict['y']][currentStockDict['x']]['fileName']

def getFileNames():
  if path.isdir(constants.DATA_PATH) is False:
    return []

  paths = listdir(constants.DATA_PATH)
  return map(lambda f: f.replace('.' + constants.FILE_EXTENSION, ''), paths)

def createFileNamesDict(fileNames):
  A_TO_Z = string.ascii_uppercase
  fileNamesDict = {}

  for char in A_TO_Z:
    array = []
    for file in fileNames:
      if file[:1].upper() == char:
        array.append(file)
    fileNamesDict[char] = array
  return fileNamesDict

def createGridPanelDict(stocksDict):
  gridPanelDict = {}
  for char in sorted(stocksDict.iterkeys()):
    if len(stocksDict[char]) > 0:
      gridPanel = vizdlg.GridPanel()
      for stocks in stocksDict[char]:
        row = []
        for stockDict in stocks:
          row.append(stockDict['radioBtn'])
          row.append(viz.addText(stockDict['fileName']))
        gridPanel.addRow(row)
      gridPanelDict[char] = gridPanel
  return gridPanelDict

def createTabPanel(gridPanelDict):
  tabPanel = vizdlg.TabPanel(skin=vizdlg.ClassicSkin)
  for key, value in sorted(gridPanelDict.iteritems()):
    tabPanel.addPanel(key, value)
  return tabPanel

def selectTab(isNext, fileNames, gridPanelDict, tabPanel, **kw):
  if len(fileNames) <= 0:
    return

  gridPanelDictKeys = sorted(gridPanelDict.iterkeys())
  idx = gridPanelDictKeys.index(getSelectedPanelChar(gridPanelDict, tabPanel))

  if isNext:
    idx += 1
    if idx >= len(gridPanelDictKeys):
      idx = 0
  else:
    idx -= 1
    if idx < 0:
      idx = len(gridPanelDictKeys) - 1

  tabPanel.selectPanel(gridPanelDict[gridPanelDictKeys[idx]])

def selectGrid(direction, fileNames, stocksDict, currentStockDictDict, **kw):
  if len(fileNames) <= 0:
    return

  currentChar = getSelectedPanelChar(**kw)
  currentStockDict = currentStockDictDict[currentChar]
  stocks = stocksDict[currentChar]

  if direction == 'right':
    currentStockDict['x'] += 1
    if currentStockDict['x'] >= len(stocks[currentStockDict['y']]):
      currentStockDict['x'] = 0
  elif direction == 'left':
    currentStockDict['x'] -= 1
    if currentStockDict['x'] < 0:
      currentStockDict['x'] = len(stocks[currentStockDict['y']]) - 1
  elif direction == 'down':
    currentStockDict['y'] += 1
    if currentStockDict['y'] >= len(stocks):
      currentStockDict['y'] = 0
  elif direction == 'up':
    currentStockDict['y'] -= 1
    if currentStockDict['y'] < 0:
      currentStockDict['y'] = len(stocks) - 1

  if direction == 'down' or direction == 'up':
    currentStockDict['x'] = min(currentStockDict['x'], len(stocks[currentStockDict['y']]) - 1)

  stocks[currentStockDict['y']][currentStockDict['x']]['radioBtn'].set(True)

def getSelectedPanelChar(gridPanelDict, tabPanel, **kw):
  selectedPanel = tabPanel.getSelectedPanel()
  for key, value in gridPanelDict.iteritems():
    if selectedPanel == value:
      return key
