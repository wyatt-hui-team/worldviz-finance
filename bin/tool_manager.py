﻿import viz
import vizconnect
import vizshape
from tools import grabber, highlighter

from utils import utils, observer, constants

class ToolManager:
  TYPE_GRABBER = 0
  TYPE_HIGHLIGHTER = 1

  def TOOL_INFO_DICTS(self):
    return [{
      'TYPE': self.TYPE_GRABBER,
      'INITIATOR': self._initGrabber,
      'WAND_TRACKER_INITIATOR': self._initGrabberWandTracker,
      'REST_FN': self._restGrabber,
      'EVENT_DICTS': [{
        'EVENT': grabber.UPDATE_INTERSECTION_EVENT,
        'CALLBACK': self._onGrabberUpdateIntersection
      }]
    }, {
      'TYPE': self.TYPE_HIGHLIGHTER,
      'INITIATOR': self._initHighlighter,
      'WAND_TRACKER_INITIATOR': self._initHighlighterWandTracker,
      'REST_FN': self._restHighlighter,
      'EVENT_DICTS': [{
        'EVENT': highlighter.HIGHLIGHT_EVENT,
        'CALLBACK': self._onHighlighterHighlight
      }],
      'PRE_EULER': constants.R_HAND_PRE_EULER,
      'PRE_TRANS': constants.R_HAND_PRE_TRANS
    }]

  def __init__(self, toolIdx, stockManager):
    self._stockManager = stockManager
    self._initTool(toolIdx)

  def changeTool(self, isNext):
    toolDict = self.toolDict

    TOOL_INFO_DICTS = self.TOOL_INFO_DICTS()

    if isNext:
      toolIdx = toolDict['idx'] + 1
      if toolIdx >= len(TOOL_INFO_DICTS):
        toolIdx = 0
    else:
      toolIdx = toolDict['idx'] - 1
      if toolIdx < 0:
        toolIdx = len(TOOL_INFO_DICTS) - 1

    self._initTool(toolIdx, toolDict)

  def _initTool(self, toolIdx, toolDict=None):
    self._initToolDict(toolDict)
    toolDict = self.toolDict

    TOOL_INFO_DICT = self.TOOL_INFO_DICTS()[toolIdx]
    tool = TOOL_INFO_DICT['INITIATOR']()
    for eventDict in TOOL_INFO_DICT['EVENT_DICTS']:
      toolDict['events'].append(eventDict['EVENT'])
      viz.callback(eventDict['EVENT'], eventDict['CALLBACK'])

    toolDict['tool'] = tool
    toolDict['restFn'] = TOOL_INFO_DICT['REST_FN']
    toolDict['wandTrackerLink'] = TOOL_INFO_DICT['WAND_TRACKER_INITIATOR']()
    toolDict['link'] = viz.link(utils.getRHand(), tool)
    if 'PRE_EULER' in TOOL_INFO_DICT:
      toolDict['link'].preEuler(TOOL_INFO_DICT['PRE_EULER'])
    if 'PRE_TRANS' in TOOL_INFO_DICT:
      toolDict['link'].preTrans(TOOL_INFO_DICT['PRE_TRANS'])
    toolDict['idx'] = toolIdx

    self.setItemsToTool(self._stockManager.stockContainers)
    observer.publish(constants.EVENT_TOOL_MANAGER_INIT_TOOL, TOOL_INFO_DICT['TYPE'])

  def _initToolDict(self, toolDict=None):
    if toolDict != None:
      vizconnect.getAvatar().setVisible(state = viz.OFF)

      link = toolDict['link']
      wandTrackerLink = toolDict['wandTrackerLink']
      if wandTrackerLink != None and link.valid():
        wandTrackerLink.remove()
      if link != None and link.valid():
        link.remove()

      restFn = toolDict['restFn']
      if callable(restFn):
        restFn()

      tool = toolDict['tool']
      if tool != None and tool.valid():
        tool.remove()

      for event in toolDict['events']:
        viz.callback(event, None)

    self.toolDict = {
      'idx': None,
      'link': None,
      'wandTrackerLink': None,
      'tool': None,
      'restFn': None,
      'events': [],
      'eventAttrDict': {}
    }

  def setItemsToTool(self, items):
    toolDict = self.toolDict
    tool = toolDict['tool']

    if tool != None:
      toolDict['restFn']()
      tool.setItems(items)

  def _initGrabber(self):
    usingPhysics=False
    tool = grabber.Grabber(
      usingPhysics=usingPhysics,
      usingSprings=usingPhysics,
      highlightMode=highlighter.MODE_OUTLINE
    )
    tool.setUpdateFunction(self._updateGrabber)
    return tool

  def _updateGrabber(self, tool):
    if utils.isTriggering():
      tool.grabAndHold()

  def _initGrabberWandTracker(self):
    vizconnect.getAvatar().setVisible(state=viz.ON)

  def _restGrabber(self):
    self.toolDict['tool'].release()

  def _onGrabberUpdateIntersection(self, e):
    self.toolDict['eventAttrDict'][grabber.UPDATE_INTERSECTION_EVENT] = e

  def _initHighlighter(self):
    tool = highlighter.Highlighter(
      highlightMode=highlighter.MODE_OUTLINE
    )
    tool.setUpdateFunction(self._updateHighlighter)
    return tool

  def _updateHighlighter(self, tool):
    rayCaster = tool.getRayCaster()
    ray = rayCaster.getRay()

    if utils.isTriggering():
      endColor = viz.GREEN
      startColor = viz.YELLOW
    else:
      endColor = (0, 0.5, 0)
      startColor = (0.5, 0.5, 0)

    ray.setColor(endColor, startColor)
    observer.publish(constants.EVENT_TOOL_MANAGER_HIGHLIGHTER_INTERSECT, rayCaster.intersect())
    tool.highlight()

  def _initHighlighterWandTracker(self):
    arrow = vizshape.addArrow(length=0.2, color=viz.WHITE)
    arrowLink = viz.link(utils.getRHand(), arrow)
    arrowLink.preEuler(constants.R_HAND_PRE_EULER)
    return arrow

  def _restHighlighter(self):
    self.toolDict['tool'].clear()

  def _onHighlighterHighlight(self, e):
    self.toolDict['eventAttrDict'][highlighter.HIGHLIGHT_EVENT] = e

  def getCurrentToolType(self):
    return self.TOOL_INFO_DICTS()[self.toolDict['idx']]['TYPE']
