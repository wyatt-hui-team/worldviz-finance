﻿_events = {}

def publish(id, data=None):
  global _events

  if _events.get(id) is None:
    return

  length = len(_events[id])

  while length != 0:
    length -= 1
    _events[id][length](data)

def subscribe(id, method, usePrepend=False):
  global _events

  if _events.get(id) is None:
    _events[id] = [method]
  else:
    if usePrepend:
      _events[id].insert(0, method)
    else:
      _events[id].append(method)

  return {
    'eventID': id,
    'method': method
  }

def unsubscribe(subscription):
  global _events

  eventID = subscription.get('eventID')
  method = subscription.get('method')

  if eventID is None or method is None:
    return

  if _events.get(eventID) is None:
    return

  index = _events[eventID].index(method)

  if index >= 0:
    del _events[eventID][index]
