﻿import math
from calendar import monthrange
from datetime import timedelta

import viz
import vizconnect

import constants

def showDialog(dialog, acceptedFn=None, cancelledFn=None):
  yield dialog.show()

  if dialog.accepted and callable(acceptedFn):
    acceptedFn(dialog)
  elif callable(cancelledFn):
    cancelledFn(dialog)

def monthdelta(d1, d2):
  delta = 0
  while True:
    mdays = monthrange(d1.year, d1.month)[1]
    d1 += timedelta(days=mdays)
    if d1 <= d2:
      delta += 1
    else:
      break
  return delta

def getRHand():
  R_HAND = constants.R_HAND

  hands = vizconnect.getAvatar().getHands()
  if R_HAND in hands:
    return hands[R_HAND]

def isTriggering():
  isTriggering = False
  if constants.BASE_VIZCONNECT_CONFIG == 'CORNER_CAVE' and getRHandInput().isButtonDown(6):
    isTriggering = True
  if viz.mouse.getState() == viz.MOUSEBUTTON_LEFT:
    isTriggering = True
  return isTriggering

def getRHandInput():
  return vizconnect.getConfiguration().getRawDict('input')['r_hand_input']

def countDigits(value):
  return int(math.log10(value)) + 1

def frange(x, y, jump):
  list = []
  while x < y:
    list.append(x)
    x += jump
  return list
